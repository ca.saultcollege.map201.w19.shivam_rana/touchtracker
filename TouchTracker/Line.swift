//
//  Line.swift
//  TouchTracker
//
//  Created by Shivam Rana on 2019-02-07.
//  Copyright © 2019 Bing Nerd Ranch. All rights reserved.
//

import CoreGraphics
import Foundation

struct Line {
    var begin = CGPoint.zero
    var end = CGPoint.zero
    
}
